import Home from '../pages/home/Home';
import About from '../pages/about/About';
import User from '../pages/user/User';

export default [
  { path: "/", key:'1', name: "Home", component: Home },
  { path: "/about", key:'2',  name: "About", component: About },
  { path: "/user",  key:'3', name: "User", component: User, auth: true },
]