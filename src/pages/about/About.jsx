import React, { Component } from 'react'

export default class About extends Component {
  constructor(props){
    super(props)
    this.state = {
      list:[1,2,3,4]
    }
  }
  render() {
    let array = this.state.list;
    let newArray = [...array];
    newArray.push(4);
    console.log(array, newArray);
    return (
      <div>
        我是about页面
        {this.state.list.map((val,i)=>{
          return <p>{i}.{val}</p>
        })}
      </div>
    )
  }
}
