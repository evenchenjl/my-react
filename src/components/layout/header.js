import React,{useEffect,useState} from 'react';
import { Menu  } from 'antd';
import { Link } from 'react-router-dom';

import { withRouter } from 'react-router'

const Head = (props) => {
  const { location } = props
  return(
    <div className="manage-header">
      <Menu theme="light" mode="horizontal" defaultSelectedKeys={location.pathname} selectedKeys={location.pathname}>
        <Menu.Item key="/">
          <Link to="/" replace>Home</Link>
        </Menu.Item>
        <Menu.Item key="/about">
          <Link to="/about" replace>About</Link>
        </Menu.Item>
        <Menu.Item key="/user">
          <Link to="/user" replace>User</Link>
        </Menu.Item>
        <Menu.Item key="/user1">
          <Link to="/user1" replace>404</Link>
        </Menu.Item>
      </Menu>
    </div>
  )
}
export default withRouter(Head)