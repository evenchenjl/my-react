import React from 'react';
import { Breadcrumb,PageHeader } from 'antd';
import { Link } from 'react-router-dom';

import { withRouter } from 'react-router'

const BMenu = (props) => {
  console.log(props);
  const breadcrumbNameMap = {
    '/': 'Home',
    '/user': 'User',
    '/about': 'About',
    '/user1': '404',
  };
  const { location } = props;
  const pathSnippets = location.pathname.split('/').filter(i => i);
  const extraBreadcrumbItems = pathSnippets.map((_, index) => {
    const url = `/${pathSnippets.slice(0, index + 1).join('/')}`;
    return (
      <Breadcrumb.Item key={url}>
        <Link to={url}>{breadcrumbNameMap[url]}</Link>
      </Breadcrumb.Item>
    );
  });
  const breadcrumbItems = [
    <Breadcrumb.Item key="home">
      <Link to="/">Home</Link>
    </Breadcrumb.Item>,
  ].concat(extraBreadcrumbItems);
  const breadRoutes = {
    routes: [
      {
        path: '/',
        breadcrumbName: 'Home',
      },
      {
        path: '/user',
        breadcrumbName: 'User',
      },
      {
        path: '/about',
        breadcrumbName: 'About',
      },
      {
        path: '/user1',
        breadcrumbName: '404',
      },
    ],
    itemRender:(route,params,routes,paths)=>{
      const secondRoute = routes.indexOf(route) === 1
      return secondRoute ? (
        <Link to={route.path}>{route.breadcrumbName}</Link>
      ) : <span>{route.breadcrumbName}</span>
    }
  }
  const breadcrumbs = ()=>{
    return (
      <Breadcrumb className="breadcrumb">
        {breadcrumbItems}
      </Breadcrumb>
    )
  }
  // console.log(props);
  return(
    // <Breadcrumb className="breadcrumb">
    //   {breadcrumbItems}
    // </Breadcrumb>
    <PageHeader
      className="site-page-header"
      title="Title"
      breadcrumb={breadRoutes}
      breadcrumbRender={breadcrumbs}
      subTitle="This is a subtitle"
    />
  )
}
export default withRouter(BMenu)