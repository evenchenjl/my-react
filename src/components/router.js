import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect 
} from 'react-router-dom';
import Routers from '../router/router'
import NotFound from '../pages/404/NotFound';
import { observer,inject } from 'mobx-react'
@inject('token')
@observer
export default class RouterSetting extends React.Component {
  constructor(props){
    super(props)
  }
  render() {
    const { token } = this.props
    return (
      <Switch>
        {Routers.map((item, index) => {
          return <Route key={index} path={item.path} exact render={props =>
            (!item.auth ? (<item.component {...props} />) : (token ? <item.component {...props} /> : <Redirect to={{
              pathname: '/login',
              state: { from: props.location }
            }} />)
            )} />
        })}
        <Route path="*" component={NotFound} />
      </Switch>
    )
  }
}