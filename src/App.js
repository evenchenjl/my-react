import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect 
} from 'react-router-dom';
import Routers from './router/router'
import './App.css';


import stores from './store/store'
//严格模式
import { Provider } from 'mobx-react'
import { configure } from "mobx"
configure({enforceActions:true})

import { Layout,Breadcrumb  } from 'antd';
const { Header, Content, Sider } = Layout;
import NotFound from './pages/404/NotFound';
import Head from './components/layout/header'
import SiderMenu from './components/layout/menu'
import RouterSetting from './components/router'
import BMenu from './components/layout/BMenu'
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    let token = stores.token
    return (
      <Provider {...stores}>
        <Router basename={window.__POWERED_BY_QIANKUN__ ? '/react17' : '/'}>
          <div className="App">
            <Layout className="App-layout">
              <Sider className="sider">
                <SiderMenu></SiderMenu>
              </Sider>
              <Layout className="layout">
                <Header className="header">
                  <Head></Head>
                </Header>
                <Layout className="content-layout">
                  <BMenu/>
                  <Content className="content">
                    <RouterSetting></RouterSetting>
                  </Content>
                </Layout>
              </Layout>
            </Layout>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
